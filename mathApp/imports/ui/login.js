import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Challenge } from '../api/challenge.js';

import './login.html';
 regUser = false;
 
 Template.login.onCreated(function helloOnCreated() {
  // counter starts at 0
    this.newUser = new ReactiveVar( false );
});
Template.login.helpers({
newUser(){
return Template.instance().newUser.get();

}
});

Template.login.events({
'click .register':function(event){
  event.preventDefault();
  Template.instance().newUser.set(true);
  


},
'submit .loginUser':function(event){
    // Get value from form element
    const target = event.target;
    const username = target.email.value;
 const password = target.password.value;
  
  Meteor.loginWithPassword(username, password);
 

}
 

});