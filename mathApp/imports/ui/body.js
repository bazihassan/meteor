import { Template } from 'meteor/templating';
//import { ReactiveVar } from 'meteor/reactive-var';
import { Challenge } from '../api/challenge.js';
import '../partials/header.html';
import './login.js';
import './body.html';
import './home.html';
import './addQuestion.html';




Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0 for number of allowed hints
  this.counter = new ReactiveVar(0);
  // used session to skip previous questions
  Session.set("questionNo", 0);
  
});

Template.hello.helpers({
  questions() {
    
   return Challenge.find({},{limit:1,skip:Session.get("questionNo")});

  }
  
   
});

Template.hello.events({
 'submit .response':function(event){
	event.preventDefault()
	var answer = event.target.answer.value;

	var playerId = this._id;
	var correctAnswer = Challenge.findOne({_id:playerId});
    if(correctAnswer.answer == answer){
     Session.set("questionNo" , Session.get("questionNo")+ 1); 
   
	}
	
   
 },
 'click .hint':function(event,template){
  
 
  
  // increment reactive-var counter
  
  // assign current counter value to hintNo
  hintNo = Template.instance().counter.get();
  
  // id number of the submitting record
  questionId = this._id;
  
  // get the node the triggered the action
 button = event.target;
 // get button parent node --- button is inside a div element
 var div = event.target.parentNode;
 // get the parent of the div eleme
 var panel = div.parentNode;

 
 // fetch records matching the given id number
 var Hints = Challenge.find({_id:questionId}).fetch();
 
 
// create new dom element
  newEl = document.createElement("p");
  newEl.setAttribute("class", "extraHint");
  // only allow 3 hints
  if(hintNo <= 2){
  msg = document.createTextNode(Hints[0].hints[hintNo].hint);
  }else {
   msg = document.createTextNode("No more Hints");
   button.disabled=true;
   
  }
	newEl.appendChild(msg);
	panel.appendChild(newEl);
	
	// increment HitNo value
	template.counter.set(template.counter.get() + 1);
	}
 

});
