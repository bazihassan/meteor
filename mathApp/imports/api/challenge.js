import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { AutoForm } from 'meteor/aldeed:autoform';
export const Challenge = new Mongo.Collection('challenge');
challenge = Challenge;
Hint = new SimpleSchema({
	number:{
	type:Number
	},
	hint:{
		type:String
	}


});
challenge.attachSchema(new SimpleSchema({
  question: {
    type: String,
    label: "Question",
    max: 200
  },
  answer: {
    type: Number,
    label: "Answer"
  },
  hints:{
	type:[Hint]
	},
}));

