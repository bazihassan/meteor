

FlowRouter.route( '/', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'login' } ); 
  },
  name: 'termsOfService'
});
FlowRouter.triggers.enter([function(context, redirect){
	if(!Meteor.userId()){
	console.log("Not logged in");
		FlowRouter.go('/');
	}


}]);


FlowRouter.route( '/home', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'home' } ); 
  },
  name: 'termsOfService'
});
FlowRouter.route( '/math', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'hello' } ); 
  },
  name: 'Math'
});
FlowRouter.route( '/admin', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'addQuestion' } ); 
  },
  name: 'Math'
});